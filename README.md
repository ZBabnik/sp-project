# RadioTube 
 

## Opis ideje  

 ---

_Disclaimer (vse osebe so fiktivne)_  
 Profesor Univerze v Ljubljani, ki poučuje na Fakulteti za Računalništvo in Informatiko dr. Vladimir Vladimirjev, dnevno preživi 
okoli dve uri v svojem avtu, ko opravlja pot od doma do fakultete. Seveda bi iz tega sklepali da živi daleč od svoje končne destinacije
v Ljubljani, a temu ni tako za dolžino poti je kriva "predobra" infrastruktura slovenskih cest ter visoko število avtomobilov v zgodnjih urah.
Torej kaj početi med polžjo vožnjo kolone, ki vodi v Ljubljano. Iskaže se da je profesor velik ljubitelj glasbe in svoje sicer nemirne živce miri
z glasbo, najraje posluša glasbo sedemdesetih in osemdesetih let prejšnjega stoletja. Nažalost pa je glasba, ki se danes vrti po radijskih medijih
zanj porazna in ker neželi poslušati pol leta iste štiri skladbe, se je nekega dne med predavanji odločil zadati svojim študentom nalogo. Izdelati
internetni radio, ta bi mu omogočal kreirati svojo postajo, ki bi vrtela glasbo po njegovi želji, pravtako pa bi lahko poslušal glasbo somišljenikov.

## Uvod v funkcionalnosti 

 --- 

Projekt _RadioTube_ je preprosta implementacija internetnega radija, ki omogoča sledeče:

 + Kreiranje računa s polnim imenom, uporabniškim imenom in geslom 
 + Obladovanje svojega "playlista": 

    + Preimenovanje seznama
    + Dodajanje novih posnetkov 
    + Odstranjevanje posnetkov 
    + Predvanjanje playlista kot lastne postaje  

 + Pridružitev v že obstoječo postajo na sledeča načina: 

    + Preko master/detail vzorca, ki uredi postaje po žanrih 
    + Preko iskalnika, ki išče po imenih postaj   

 + Prikaz in urejanje svojega profila 

## Opis zaslonskih mask in povezav med njimi 

 --- 

### __Registracija računa__ 

Preden lahko uporabniki uporabljajo storitve aplikacije _RadioTube_
morajo izpolniti obrazec za registracijo, ki vsebuje okna za:

 + __Polno Ime__ _(Full name)_
 + __Uporabniško ime__ _(Username)_ 
 + __Geslo računa__ _(Password)_ 
 + __Potrditev gesla__ _(Re-enter password)_ 

Ko se uporabniki uspešno registrirajo jih stran preusmeri [na stran za vpis](#markdown-header-prijava-v-aplikacijo)

![Tukaj pride zaslonska maska za registracijo](./docs/spo-signup.png) 

![Tukaj pride zaslonska maska za registracijo](./docs/spo-signup2.png)  

### __Prijava v aplikacijo__ 

Če imajo uporabniki že narejen račun za aplikacijo, ki ga lahko naredijo 
preko klika na gumb [Sign Up](#markdown-header-registracija-računa), se lahko tu prijavijo
v aplikacijo z vpisom:
 
 + __Uporabniškega imena__ _(username)_ 
 + __Gesla__ _(password)_ 

V primeru uspešne prijave stran uporabnika preusmeri na [domačo stran uporabnika](#markdown-header-stran-uporabnika)

![Tukaj pride zaslonska maska za vpis](./docs/spo-login.png) 

![Tukaj pride zaslonska maska za vpis](./docs/spo-login2.png) 

### __Stran uporabnika__ 

 Na domači strani se uporabniku prikažeta dve okni, prvo stransko prikazuje sliko uporabnika(bomo sploh mel sliko? 
al bo kr placeholder), pod sliko pa se nahajajo še trije gumbi za navigacijo po spletni strani :

 + [__Profile__](#markdown-header-urejanje-profila) 
 + [__Stations__](#markdown-header-stran-uporabnika) 
 + [__Playlist__](#markdown-header-urejanje-seznama-predvajanja) 

 Poleg stranskega okna, pa imamo tu še glavno okno, ki zaseda večino zaslona. Tu se generira Master/detail vzorec 
po žanrih, pri čemer se ob kliku generira podseznam z imeni radijskih postaj _"oz.stationov"_, ki so vključene
v izbran žaner. V tem oknu se nahaja tudi iskalno okno v katerega lahko vpišemo ime željene postaje. V primeru
uspešnega iskanja oz. ob kliku na postajo v Master/detail vzorcu nas stran preusmeri na [domači zaslon postaje](#markdown-header-zaslon-postaje)

![Tukaj pride zaslonska maska domače strani](./docs/spo-home.png)

 V primeru mobilne aplikacije lahko do stranskega okna dostopamo, preko klika na gumb za __Skritje/Prikaz__ _(Show/Hide)_, kakor prikazujeta 
 spodnji dve zaslonski maski.

![Tukaj pride zaslonska maska domače strani](./docs/spo-home2.png) 

![Tukaj pride zaslonska maska univerzalne strani](./docs/spo-dropdown2.png)

### __Urejanje profila__  
 
Na strani uporabniškega profila se uporabniku v glavnem oknu izpišejo sledeči podatki: 

 + __Polno ime__ _(Full Name)_ 
 + __Uporabniško ime__ _(Username)_ 
 + __Geslo__ _(Password)_ 
 + __Slika profila__ _(Picture)_ 

 Uporabniki lahko podatke spreminjajo s klikom na gumb change, po čemer se prikaže vpisno okno v katerega lahko vtipkajo nove podatke, če so podatki
skladni se od kliku na gumb __Shrani__ _(Save)_ spremenijo/posodobijo v podatkovni bazi.

![Tukaj pride zaslonska maska urejanja profila](./docs/spo-profile.png)  

![Tukaj pride zaslonska maska urejanja profila](./docs/spo-profile2.png)

### __Urejanje seznama predvajanja__ 

 Na strani našega seznama predvajanja, se nam v glavnem oknu aplikacije prikaže seznam vseh skladb/video posnetkov, ki smo jih že dodali v seznam.
V primeru da želimo ustvariti nov zapis, potrebuje uporabnik klikniti na gumb __Dodaj__ _(Add)_, ob kliku se odpre pojavno okno v katerega lahko prilepimo
YouTube-URL željenega posnetka.  

 Na tej strani lahko s seznama tudi brišemo zapise, dokaj preprosto s klikom na gumb v obliki _koša za smeti_, ki se pojavi ob
vsakem zapisu na listu, tako se ob kliku izbriše isto ležeči zapis s seznama. Stran nam omogoča tudi preimenovanje imena seznama predvajanja, s klikom na gumb
__Preimenuj__ _(Rename)_, se nam odpre novo pojavno okno v katerega lahko vpišemo novo ime. 

 Poleg tega je tu še gumb __PredvajajDrugim__ _(GoLive)_,
ki ustvari radijsko postajo z istim imenom kot ga ima seznam predvajanja. Ob kliku pa stran uporabnika preusmeri na [domači zaslon njegove postaje](#markdown-header-zaslon-postaje), 
saj postaje delujejo samo v primeru, da je med poslušalci tudi lastnik seznama predvajanja. (Preprečujemo, nastanek kupa postaj brez poslušalcev.)

![Tukaj pride zaslonska maska urejanja seznama predvajanja](./docs/spo-playlists.png) 

![Tukaj pride zaslonska maska urejanja seznama predvajanja](./docs/spo-playlists2.png) 

### __Zaslon postaje__ 

 Na domači strani postaje se v glavnem oknu izpiše ime postaje, ki smo se ji pridružili(v primeru da je to naša postaja se izpiše ime našega seznama predvajanja), pod
imenom se pojavi predvajalnik posnetkov, ki predvaja skladbe/video posnetke, ki so navedeni pri strani v razdelku __Seznam predvajanja postaje__ _(Station Playlist)_

![Tukaj pride zaslonska maska postaje](./docs/spo-station.png)

![Tukaj pride zaslonska maska postaje](./docs/spo-station2.png)

## Opis podatkovne baze in API-ja 

 --- 

### Podatkovna baza 

 Podatkovna baza bo za vsakega od uporabnikov hranila osnovne podatke, poleg tega pa še seznam, ki predstavlja seznam predvajanja. V seznamu bodo shranjeni 
enolični identifikatorji YouTube videoposnetkov. Npr. za videoposnetek _FRIdom - while(True) {...}_, ki se nahaja na naslovu https://www.youtube.com/watch?v=qozTkCHexH8
se bo hranil le niz qozTkCHexH8 saj ta enolično identificira navedeni videoposnetek, pravtako pa je tak način primeren za uporabo YouTube API-ja.

### YouTube API

 V projektu _RadioStation_ bomo uporabljali YouTubov API, s pomočjo katerega bomo v iframe-u predvajali videoposnetke, s seznama predvajanja postaje. Več informacij
o omenjenem api-ju lahko zasledimo [na tej povezavi](https://developers.google.com/youtube/iframe_api_reference).


### Vhodna spletna stran

 ---

Vhodna spletna stran se nahaja na naslovu:
 https://workspace-username.c9users.io/

Stran ki se naloži pa je:
 signin.jade

### Opis razlik med prikazi

 ---
 
 Delovanje aplikacije smo preverili v brskalnikih Chrome, Firefox in Edge. Opazili smo manjša razlikovanja:
 
 + Chrome je imel pri vnosnem polju za geslo manjše pikice kot ostala brskalnika.
 + Pri select meniju se Chrome in Firefox vedno razširita navzdol, Edge pa se razširi v obe smeri glede na izbrani element.
 + Chrome ima manjši placeholder text kot ostala brskalnika.
 + Pri Chromu in Firefoxu se navigacijska vrstica premika skupaj s stranjo navzdol, medtem ko pri Edgu ostane na vrhu strani.
 


### Povezava do Heroku
 
 ---
 
 Aplikacijo _RadioTube_ najdemo na naslovu:
 
 https://radiotube-sp.herokuapp.com/
 
 

### Navodila za namestitev in zagon vaše aplikacije
 
 ---
 
 Če želimo v c9 okolju zagnati aplikacijo moramo prvo namestiti MongoDB.
 Kloniramo repozitorij iz bitbucket naslova:
 
 + https://ZBabnik@bitbucket.org/ZBabnik/sp-project.git
 + git@bitbucket.org:ZBabnik/sp-project.git
 
 Preden zaženemo aplikacijo moramo pognati bazo:

 + $ cd mongodb
 + $ ./mongod
 
 Odpremo nov terminal in se premaknemo v mapo in namestimo npm:

 + $ cd radiotube 
 + $ npm install
 
 Zaženemo aplikacijo z ukazom:

 + $ node ./bin/www


 Da omogočimo aplikaciji normalno delovanje moramo v bazo zapisati podatke.
 Navigiramo na _/db_ , kjer lahko pobrišemo vse podatke in vstavimo nove. Podatke in njihovo obliko najdemo v datoteki _db-init.json_ .
 Podatke kopiramo v za njih ustrezna vnosna polja in pritisnemo gumb _add_ . Nato se prestavimo na začetno stran _/_ kjer se lahko vpišemo kot testni primer: _test_ _test_ .
 

### Izmerjeni časi nalaganja
 
 ---
 
#### chrome
+ /		1.19s
+ /signup		1.06s
+ /playlist	1.62s
+ /stations 	1.49s
+ /profile	1.71s
+ /station/id	4.56s

#### edge
+ / 2.60s		
+ /signup 1.63s
+ /playlist	2.43s
+ /stations 	2.89s
+ /profile	2.41s
+ /station/id	5.52s

Na obeh brskalnikih je najpočasnejša stran /station/id. Najpočasnejša je, ker se na njej predvajajo video posnetki iz youtuba.

### Selenium

Selenium test smo izvedli na lokalnem okolju, s pomočjo orodja Katalon Recorder. Izvožena skripta je _seleniumTest.py_ .

### JMeter

![jmeter graf](./docs/jmeter.png)


# ZAP Scanning Report




## Summary of Alerts

| Risk Level | Number of Alerts |
| --- | --- |
| High | 0 |
| Medium | 1 |
| Low | 5 |
| Informational | 0 |

## Alert Detail

  
  
### X-Frame-Options Header Not Set
##### Medium (Medium)
  
  
  
  
#### Description
<p>X-Frame-Options header is not included in the HTTP response to protect against 'ClickJacking' attacks.</p>
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `X-Frame-Options`
  
  
  
  
Instances: 1
  
### Solution
<p>Most modern Web browsers support the X-Frame-Options HTTP header. Ensure it's set on all web pages returned by your site (if you expect the page to be framed only by pages on your server (e.g. it's part of a FRAMESET) then you'll want to use SAMEORIGIN, otherwise if you never expect the page to be framed, you should use DENY. ALLOW-FROM allows specific websites to frame the web page in supported web browsers).</p>
  
### Reference
* http://blogs.msdn.com/b/ieinternals/archive/2010/03/30/combating-clickjacking-with-x-frame-options.aspx

  
#### CWE Id : 16
  
#### WASC Id : 15
  
#### Source ID : 3

  
  
  
### Cookie Without Secure Flag
##### Low (Medium)
  
  
  
  
#### Description
<p>A cookie has been set without the secure flag, which means that the cookie can be accessed via unencrypted connections.</p>
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `YSC`
  
  
  * Evidence: `Set-Cookie: YSC`
  
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `VISITOR_INFO1_LIVE`
  
  
  * Evidence: `Set-Cookie: VISITOR_INFO1_LIVE`
  
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `PREF`
  
  
  * Evidence: `Set-Cookie: PREF`
  
  
  
  
Instances: 3
  
### Solution
<p>Whenever a cookie contains sensitive information or is a session token, then it should always be passed using an encrypted channel. Ensure that the secure flag is set for cookies containing such sensitive information.</p>
  
### Reference
* http://www.owasp.org/index.php/Testing_for_cookies_attributes_(OWASP-SM-002)

  
#### CWE Id : 614
  
#### WASC Id : 13
  
#### Source ID : 3

  
  
  
### Incomplete or No Cache-control and Pragma HTTP Header Set
##### Low (Medium)
  
  
  
  
#### Description
<p>The cache-control and pragma HTTP header have not been set properly or are missing allowing the browser and proxies to cache content.</p>
  
  
  
* URL: [https://www.youtube.com/yts/cssbin/www-player-webp-vflP-JRqm.css](https://www.youtube.com/yts/cssbin/www-player-webp-vflP-JRqm.css)
  
  
  * Method: `GET`
  
  
  * Parameter: `Cache-Control`
  
  
  * Evidence: `public, max-age=31536000`
  
  
  
  
* URL: [https://www.youtube.com/annotations_invideo?cap_hist=1&video_id=Uk1d9bUTJxA&client=56&ei=IHdaWoiCJMGA1gKbwbegCA](https://www.youtube.com/annotations_invideo?cap_hist=1&video_id=Uk1d9bUTJxA&client=56&ei=IHdaWoiCJMGA1gKbwbegCA)
  
  
  * Method: `POST`
  
  
  * Parameter: `Cache-Control`
  
  
  * Evidence: `no-cache`
  
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `Cache-Control`
  
  
  * Evidence: `no-cache`
  
  
  
  
Instances: 3
  
### Solution
<p>Whenever possible ensure the cache-control HTTP header is set with no-cache, no-store, must-revalidate; and that the pragma HTTP header is set with no-cache.</p>
  
### Reference
* https://www.owasp.org/index.php/Session_Management_Cheat_Sheet#Web_Content_Caching

  
#### CWE Id : 525
  
#### WASC Id : 13
  
#### Source ID : 3

  
  
  
### Private IP Disclosure
##### Low (Medium)
  
  
  
  
#### Description
<p>A private IP (such as 10.x.x.x, 172.x.x.x, 192.168.x.x) or an Amazon EC2 private hostname (for example, ip-10-0-56-78) has been found in the HTTP response body. This information might be helpful for further attacks targeting internal systems.</p>
  
  
  
* URL: [https://www.youtube.com/yts/jsbin/player-vflLCGcm0/en_GB/base.js](https://www.youtube.com/yts/jsbin/player-vflLCGcm0/en_GB/base.js)
  
  
  * Method: `GET`
  
  
  * Evidence: `10.01.09.16`
  
  
  
  
Instances: 1
  
### Solution
<p>Remove the private IP address from the HTTP response body.  For comments, use JSP/ASP/PHP comment instead of HTML/JavaScript comment which can be seen by client browsers.</p>
  
### Other information
<p>10.01.09.16</p><p>10.01.09.99</p><p></p>
  
### Reference
* https://tools.ietf.org/html/rfc1918

  
#### CWE Id : 200
  
#### WASC Id : 13
  
#### Source ID : 3

  
  
  
### Cookie No HttpOnly Flag
##### Low (Medium)
  
  
  
  
#### Description
<p>A cookie has been set without the HttpOnly flag, which means that the cookie can be accessed by JavaScript. If a malicious script can be run on this page then the cookie will be accessible and can be transmitted to another site. If this is a session cookie then session hijacking may be possible.</p>
  
  
  
* URL: [https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1](https://www.youtube.com/embed/Uk1d9bUTJxA?controls=0&autoplay=1&enablejsapi=1&origin=http%3A%2F%2Flocalhost%3A3000&widgetid=1)
  
  
  * Method: `GET`
  
  
  * Parameter: `PREF`
  
  
  * Evidence: `Set-Cookie: PREF`
  
  
  
  
Instances: 1
  
### Solution
<p>Ensure that the HttpOnly flag is set for all cookies.</p>
  
### Reference
* http://www.owasp.org/index.php/HttpOnly

  
#### CWE Id : 16
  
#### WASC Id : 13
  
#### Source ID : 3

  
  
  
### Cross-Domain JavaScript Source File Inclusion
##### Low (Medium)
  
  
  
  
#### Description
<p>The page includes one or more script files from a third-party domain.</p>
  
  
  
* URL: [http://localhost:3000/playlist/](http://localhost:3000/playlist/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/bootstrap/](http://localhost:3000/bootstrap/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/api/v1](http://localhost:3000/api/v1)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/api/v1/stations](http://localhost:3000/api/v1/stations)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/lib/](http://localhost:3000/lib/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/stations/](http://localhost:3000/stations/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/javascript/](http://localhost:3000/javascript/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/stylesheets/](http://localhost:3000/stylesheets/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/api](http://localhost:3000/api)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/station/%7B%7B%20stationP2.userID%20%7D%7D](http://localhost:3000/station/%7B%7B%20stationP2.userID%20%7D%7D)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/robots.txt](http://localhost:3000/robots.txt)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/images/](http://localhost:3000/images/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/angular/](http://localhost:3000/angular/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/signup/](http://localhost:3000/signup/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/signin/](http://localhost:3000/signin/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/profile/](http://localhost:3000/profile/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/playlist/modals/](http://localhost:3000/playlist/modals/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/](http://localhost:3000/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/bootstrap/js/](http://localhost:3000/bootstrap/js/)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
* URL: [http://localhost:3000/sitemap.xml](http://localhost:3000/sitemap.xml)
  
  
  * Method: `GET`
  
  
  * Parameter: `https://www.youtube.com/iframe_api`
  
  
  * Evidence: `<script src="https://www.youtube.com/iframe_api"></script>`
  
  
  
  
Instances: 30
  
### Solution
<p>Ensure JavaScript source files are loaded from only trusted sources, and the sources can't be controlled by end users of the application.</p>
  
### Reference
* 

  
#### CWE Id : 829
  
#### WASC Id : 15
  
#### Source ID : 3





