# -*- coding: utf-8 -*-
from selenium import webdriver
from selenium.webdriver.common.by import By
from selenium.webdriver.common.keys import Keys
from selenium.webdriver.support.ui import Select
from selenium.common.exceptions import NoSuchElementException
from selenium.common.exceptions import NoAlertPresentException
import unittest, time, re

class Test1(unittest.TestCase):
    def setUp(self):
        self.driver = webdriver.Firefox()
        self.driver.implicitly_wait(30)
        self.base_url = "https://www.katalon.com/"
        self.verificationErrors = []
        self.accept_next_alert = True
    
    def test_1(self):
        driver = self.driver
        driver.get("http://localhost:3000/")
        driver.find_element_by_id("signin-submit").click()
        driver.find_element_by_link_text("Sign Up").click()
        driver.find_element_by_id("name").click()
        driver.find_element_by_id("name").clear()
        driver.find_element_by_id("name").send_keys("selenium test")
        driver.find_element_by_id("userName").clear()
        driver.find_element_by_id("userName").send_keys("selenium")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("confirm").clear()
        driver.find_element_by_id("confirm").send_keys("test")
        driver.find_element_by_id("signup-submit").click()
        driver.find_element_by_id("username").click()
        driver.find_element_by_id("username").clear()
        driver.find_element_by_id("username").send_keys("selenium")
        driver.find_element_by_id("password").clear()
        driver.find_element_by_id("password").send_keys("test")
        driver.find_element_by_id("password").send_keys(Keys.ENTER)
        driver.find_element_by_id("playlistsButtonMain").click()
        driver.find_element_by_id("playlistButtonToolbar-rename").click()
        driver.find_element_by_id("playlist-rename-input").click()
        driver.find_element_by_id("playlist-rename-input").clear()
        driver.find_element_by_id("playlist-rename-input").send_keys("Super playlista")
        driver.find_element_by_id("renamePlaylist").click()
        driver.find_element_by_id("playlistButtonToolbar-add").click()
        driver.find_element_by_id("playlist-add-input").click()
        driver.find_element_by_id("playlist-add-input").clear()
        driver.find_element_by_id("playlist-add-input").send_keys("https://www.youtube.com/watch?v=ItiSiLLZ7jU")
        driver.find_element_by_id("addVideo").click()
        driver.find_element_by_id("playlistButtonToolbar-golive").click()
        driver.find_element_by_id("playlist-genre-select").click()
        Select(driver.find_element_by_id("playlist-genre-select")).select_by_visible_text("Rock")
        driver.find_element_by_xpath("//option[@value='Rock']").click()
        driver.find_element_by_name("stationTag").click()
        Select(driver.find_element_by_name("stationTag")).select_by_visible_text("Fun")
        driver.find_element_by_xpath("//option[@value='Fun']").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_id("stationsButtonMain").click()
        driver.find_element_by_id("Rock").click()
        driver.find_element_by_id("search_par").click()
        driver.find_element_by_id("search_par").clear()
        driver.find_element_by_id("search_par").send_keys("super")
        driver.find_element_by_id("playlistsButtonMain").click()
        driver.find_element_by_id("playlistsButtonMain").click()
        driver.find_element_by_id("playlistButtonToolbar-golive").click()
        driver.find_element_by_xpath("//button[@type='submit']").click()
        driver.find_element_by_id("stationsButtonMain").click()
        driver.find_element_by_id("search_par").click()
        driver.find_element_by_id("search_par").clear()
        driver.find_element_by_id("search_par").send_keys("sup")
        driver.find_element_by_id("search_par").send_keys(Keys.ENTER)
        driver.find_element_by_id("search_par").clear()
        driver.find_element_by_id("search_par").send_keys("")
    
    def is_element_present(self, how, what):
        try: self.driver.find_element(by=how, value=what)
        except NoSuchElementException as e: return False
        return True
    
    def is_alert_present(self):
        try: self.driver.switch_to_alert()
        except NoAlertPresentException as e: return False
        return True
    
    def close_alert_and_get_its_text(self):
        try:
            alert = self.driver.switch_to_alert()
            alert_text = alert.text
            if self.accept_next_alert:
                alert.accept()
            else:
                alert.dismiss()
            return alert_text
        finally: self.accept_next_alert = True
    
    def tearDown(self):
        self.driver.quit()
        self.assertEqual([], self.verificationErrors)

if __name__ == "__main__":
    unittest.main()
