var passport = require('passport');
var LocalStrategy = require('passport-local').Strategy;
var mongoose = require('mongoose');
var User = mongoose.model('User');

passport.use(new LocalStrategy({
     usernameField: 'userName',
     passwordField: 'password'
   },
   function(userName, password, finished) {
     User.findOne(
       { userName: userName }, 
       function(error, user) {
         if (error) { 
             return finished(error); 
         }
         if (!user) {
           return finished(null, false, {
             error: 'Wrong username or password!'
           });
         }
         
         if (!user.checkPassword(password)) {
           return finished(null, false, {
             error: 'Wrong username or password!'
           });
         }
         return finished(null, user);
       }
     );
   }
));