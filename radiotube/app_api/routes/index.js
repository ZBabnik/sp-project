var express = require('express');
var router = express.Router();
var ctrlUsers = require("../controllers/users.js");
var ctrlPlaylist = require("../controllers/playlists.js");
var ctrlStations = require("../controllers/stations.js");
var ctrlLogs = require("../controllers/logs.js");
var jwt = require('express-jwt');
var avtentication = jwt({
   secret: process.env.JWT_PASS,
   userProperty: 'payload'
});

//var ctrlSignUp = require("../controllers/signup.js");
var ctrlDb = require("../controllers/db.js");

/* GET home page. */
router.get('/users/:userID', ctrlUsers.getUser);

/* zahtevki za /playlist */
router.get('/playlists/:userID', avtentication, ctrlPlaylist.getPlaylist);
router.delete('/playlists/:userID/:videoID', avtentication, ctrlPlaylist.deleteVideoFromPlaylist);
router.post('/playlists/:userID', avtentication, ctrlPlaylist.addVideoToPlaylist);
router.put('/playlists/:userID', avtentication, ctrlPlaylist.changePlaylistName);
router.post('/playlistsLive/:userID', avtentication, ctrlPlaylist.goLivePlaylist);
router.delete('/playlistsLive/:userID', avtentication, ctrlPlaylist.deleteStation);

/* zahtevki za /profile */
router.put('/profile/:userID', avtentication,  ctrlUsers.updateProfile);

/* zahtevki za /stations */
router.get('/stations/:genre/:paggination', avtentication, ctrlStations.getStations);

/* signin signup */
router.post('/signin', ctrlUsers.signin);

router.post('/signup', ctrlUsers.signup);


/* db zahtevki */
router.delete('/db', ctrlDb.deleteDb);
router.post('/dbUsers', ctrlDb.addUser);
router.post('/dbGenres', ctrlDb.addGenre);
router.post('/dbTags', ctrlDb.addTag);


/* zahtevek za /signup */
router.post('/signup', ctrlUsers.addUser);


/* zahtevki za logging */
router.post('/logs', ctrlLogs.trackUser);

module.exports = router;
