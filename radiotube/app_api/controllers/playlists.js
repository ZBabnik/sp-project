var mongoose = require('mongoose');
var User = mongoose.model('User');
var Genre = mongoose.model('Genre');
var Tag = mongoose.model('Tag');
var Playlist = mongoose.model('Playlist');
var request = require('request');
var google = require('googleapis');
var youtube = google.youtube('v3');
//var googleAuth = require('google-auth-library');

var returnJSONResponse = function(res, status, data) {
    res.status(status);
    res.json(data);
};

module.exports.getPlaylist = function(req, res) {
  if(req.params && req.params.userID) {
    User
      .findById(req.params.userID)
      .exec(function(error, user) {
        if(!user) {
          returnJSONResponse(res, 404, {"error" : "error"});
          return;
        } else if(error) {
          returnJSONResponse(res, 404, error);
          return;
        }
          returnJSONResponse(res, 200, user.playlist);
        });
  } else {
      returnJSONResponse(res, 404, {"error" : "error"});
  }
};

var addVideo = function(req, res, data) {
  if (!data) {
    returnJSONResponse(res, 404, {"error": "error"});
  } else {
    var API_KEY = process.env.API_KEY;
    // Get id from link
    //https://www.youtube.com/watch?v=_zllJ2HHAVQ
    var substring = req.body.youtubeID.split("v=")[1];
    var videoName;
    
    youtube.videos.list({
      part: ["snippet"],
      auth: API_KEY,
      id: substring
    }, function(error, resYT) {
      if(error) {
        returnJSONResponse(res, 400, {
          "error": "Error requesting video!"
        });
      }
      
      if(!resYT.items || resYT.items == null || resYT.items.length == 0) {
        returnJSONResponse(res, 400, {
          "error" : "Video not found!"
        });
        return;
      }
      videoName = resYT.items[0].snippet.title;
      data.playlist.videos.push({
        videoName: videoName,
        youtubeID: substring
      });
      data.save(function(error, data) {
        var video;
        if (error) {
          returnJSONResponse(res, 400, error);
        } else {
          video = data.playlist.videos[data.playlist.videos.length - 1];
          returnJSONResponse(res, 201, video);
        }
      });
    });
  }
};

module.exports.addVideoToPlaylist = function(req, res) {
  var userID = req.params.userID;
  //console.log("A");
  //console.log(userID);
  if (userID) {
    User
      .findById(userID)
      .select('playlist')
      .exec(
        function(error, user) {
          if (error)
            returnJSONResponse(res, 400, error);
          else
            addVideo(req, res, user);
        }
      );
  } else {
    returnJSONResponse(res, 404, {"error": "error"});
  }  
};

module.exports.changePlaylistName = function(req, res) {
  var userID = req.params.userID;
  if (userID) {
    User
      .findById(userID)
      .select('playlist')
      .exec(
        function(error, data) {
          if (!data) {
           returnJSONResponse(res, 404, {"error": "error"});
           return;
          } else if (error) {
           returnJSONResponse(res, 400, error);
           return;
          }
          data.playlist.playlistName = req.body.playlistName,
          data.save(function(error, data) {
          if (error)
               returnJSONResponse(res, 404, error);
             else
               returnJSONResponse(res, 200, data);
          });
      });
  }  
};

module.exports.deleteVideoFromPlaylist = function(req, res) {
  var userID = req.params.userID;
  var videoID = req.params.videoID;
  if (userID && videoID) {
    User
      .findById(userID)
      .select('playlist')
      .exec(
        function(error, data) {
          if (!data) {
            returnJSONResponse(res, 404, {"error": "error"});
            return;
          } else if (error) {
            returnJSONResponse(res, 400, error);
            return;
          } else if(!data.playlist.videos.id(videoID)) {
            returnJSONResponse(res, 400, {"error": "error"});
            return;
          }
          data.playlist.videos.id(videoID).remove();
          data.save(function(error) {
            if (error)
              returnJSONResponse(res, 404, error);
            else
              returnJSONResponse(res, 204, null);
          });
      });
  }  
};


// ne gledat kode pod tem komentarjem ker lahko postaneš suicidal
module.exports.goLivePlaylist = function(req, res) {
  var userID = req.params.userID;
  var genre = req.body.stationGenre;
  var tag = req.body.stationTag;
  console.log(genre);
  if (userID && genre && tag) {
    User
      .findById(userID)
      .select('playlist')
      .exec(
        function(error, user) {
          if(!user) {
            returnJSONResponse(res, 404, {"error": "error"});
            return;
          } else if(error) {
            returnJSONResponse(res, 400, error);
            return;
          } else {
            if(user.playlist.station != 'F' && user.playlist.tag != 'F') { // že obstaja postaja
              returnJSONResponse(res, 400, {"error": "error"});
              return;
            } else {
              Genre
                .findOne({genreName: genre})
                .select()
                .exec(
                  function(error, genre) {
                    console.log(genre);
                    if(!genre) {
                      returnJSONResponse(res, 404, {"error": "error"});
                      return;
                    } else if(error) {
                      returnJSONResponse(res, 400, error);
                      return;
                    } else {
                      genre.stations.push({
                        stationName : user.playlist.playlistName,
                        userID : userID
                      });
                      genre.save(function(error, newGenre) {
                        if (error) {
                          returnJSONResponse(res, 400, error);
                        } else {
                          //console.log(newGenre);
                          //console.log(genre);
                          user.playlist.station = genre.stations[genre.stations.length - 1]._id;
                          Tag
                            .findOne({tagName: tag})
                            .select()
                            .exec(
                              function(error, tag) {
                                if(!tag) {
                                  returnJSONResponse(res, 404, {"error": "error"});
                                  return;
                                } else if(error) {
                                  returnJSONResponse(res, 400, error);
                                  return;
                                } else {
                                  tag.stations.push({
                                    stationName : user.playlist.playlistName,
                                    userID : userID
                                  });
                                  tag.save(function(error, newTag) {
                                    if (error) {
                                      returnJSONResponse(res, 400, error);
                                    } else {
                                      user.playlist.tag = tag.stations[tag.stations.length - 1]._id;
                                      user.save(function(error, newUser) {
                                        if(error) {
                                          returnJSONResponse(res, 400, error);
                                          return;
                                        } else {
                                          returnJSONResponse(res, 201, newUser);
                                        }
                                      }); 
                                    }
                                  });
                                }
                          }); 
                        }
                      });
                    }
              });
            }
          }
      });
  } else {
    returnJSONResponse(res, 400, {"error": "error"});
  } 
};

module.exports.deleteStation = function(req, res) {
  var userID = req.params.userID;
  if (userID) {
    User
      .findById(userID)
      .select('playlist')
      .exec(
        function(error, user) {
          if(!user) {
            returnJSONResponse(res, 404, {"error": "error"});
            return;
          } else if(error) {
            returnJSONResponse(res, 400, error);
            return;
          } else {
            Genre
              .findOne({ "stations._id" : user.playlist.station})
              .select('stations')
              .exec(
                  function(error, genre) {
                    if(!genre) {
                      returnJSONResponse(res, 404, {"error": "error"});
                      return;
                    } else if(error) {
                      returnJSONResponse(res, 400, error);
                      return;
                    } else {
                      genre.stations.id(user.playlist.station).remove();
                      genre.save(function(error) {
                        if (error) {
                          returnJSONResponse(res, 404, error);
                          return;
                        }
                        else {
                          user.playlist.station = 'F';
                          Tag
                            .findOne({ "stations._id" : user.playlist.tag})
                            .select('stations')
                            .exec(
                              function(error, tag) {
                                if(!tag) {
                                  returnJSONResponse(res, 404, {"error": "error"});
                                  return;
                                } else if(error) {
                                  returnJSONResponse(res, 400, error);
                                  return;
                                } else {
                                  tag.stations.id(user.playlist.tag).remove();
                                  tag.save(function(error) {
                                    if (error) {
                                      returnJSONResponse(res, 404, error);
                                      return;
                                    }
                                    else {
                                      user.playlist.tag = 'F';
                                      user.save(function(error) {
                                        if (error) {
                                          returnJSONResponse(res, 404, error);
                                          return;
                                        } else {
                                          returnJSONResponse(res, 204, null);
                                        }
                                      });
                                    }
                                  });
                                }
                             });
                          }
                      });
                    }
                  });
              }
        });
  } else {
    returnJSONResponse(res, 400, {"error": "error"});
  } 
};

