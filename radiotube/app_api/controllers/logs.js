var fs = require('fs');

var returnJSONResponse = function(res, status, data) {
    res.status(status);
    res.json(data);
};


//{ location: '/playlists', userID: '5a58ae0f0df2f81a6335d542' }
module.exports.trackUser = function(req, res) {
   var logString = "";
   if(req.body.location && req.body.userID) {
      logString = "["+new Date()+","+(new Date()).getHours()+"] User ID="+req.body.userID+", Location URL="+req.body.location+"\n";
   }
   else {
      console.log("Not all info sent! Failed to write log!")
      returnJSONResponse(res, 200, null); 
      return;
   }
   
   fs.appendFile(__dirname+"/logs/log.txt", logString, function(err) {
    if(err) {
        console.log("Failed writing log!"+logString);
    }
        console.log("The following log was added:"+logString);
    }); 
   
   returnJSONResponse(res, 200, null);
};