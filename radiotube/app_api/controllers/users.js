var mongoose = require('mongoose');
var User = mongoose.model('User');
var Playlist = mongoose.model('Playlist');
var passport = require('passport');

var returnJSONResponse = function(res, status, data) {
    res.status(status);
    res.json(data);
};

/*module.exports.signin = function(req, res) {
  User
    .find({ $and:[{
        userName: req.body.username
      }, {
        password: req.body.password}]
    })
    .select()
    .exec(
      function(error, user) {
        console.log(user[0]);
        if(!user) {
          returnJSONResponse(res, 500, {"error": "error"});
          return;
        } else if(error) {
          returnJSONResponse(res, 400, error);
          return;
        } else {
          if(!user[0]){
            returnJSONResponse(res, 500, {"error": "H"});
            return;
          } else {
            returnJSONResponse(res, 200, user);
          }
        }       
  });
};*/

module.exports.signin = function(req, res) {
  
  if(!req.body.userName || !req.body.password) {
    returnJSONResponse(res, 400, {
        "error" : "Submit all fields!"
    });
  }
  
  passport.authenticate('local', function(error, user, data) {
    var token;
    if(error) {
      returnJSONResponse(res, 404, error);
      return;
    }
    if(user) {
      token = user.generateJwt();
      returnJSONResponse(res, 200, {
        "token" : token
      });
    }
    else {
      returnJSONResponse(res, 401, data);
    }
  })(req, res);
};

module.exports.getUser = function(req, res) {
    if(req.params && req.params.userID) {
        User
            .findById(req.params.userID)
            .exec(function(error, user) {
                if(!user) {
                    returnJSONResponse(res, 404, {"error" : "NOT FOUND!"});
                    return;
                } else if(error) {
                    returnJSONResponse(res, 404, error);
                    return;
                }
                returnJSONResponse(res, 200, user);
        });
    } else {
        returnJSONResponse(res, 404, {"error" : "NOT FOUND!"});
    }
};

module.exports.updateProfile = function(req, res) {
    console.log(req.body);
    if(req.params && req.params.userID) {
        User
            .findById(req.params.userID)
            .select('-playlist')
            .exec(
            function(error, user) {
            if (!user) {
                returnJSONResponse(res, 400, {
                  "error": "User not found, try re-logging!"
                });
                return;
            } else if (error) {
               returnJSONResponse(res, 400, {
                 "error": "Server error, try again later!"
               });
               return;
            }
            if(req.body.fullName)
                user.fullName = req.body.fullName;
            if(req.body.userName)
                user.userName = req.body.userName;
            
            user.save(function(error, user) {
                if (error) {
                  returnJSONResponse(res, 400, {
                    "error": "Server error, try again later!"
                  });
                  return;
                }
                else {
                  returnJSONResponse(res, 200, user);
                  return;
                }
            }); 
           
        });
    } else {
        returnJSONResponse(res, 404, {
          "error": "Lost data - please try again!"
        });
    }
};

/* dont use this .... it will break the db */
module.exports.addUser = function(req, res) {
     User.create({
      fullName: req.body.fullName,
      userName: req.body.userName,
      password: req.body.password,
      playlist: req.body.userName + "'s playlist",

    }, function(error, user) {
      if (error)
        returnJSONResponse(res, 400, error);
      else
        returnJSONResponse(res, 201, user);
    });
};

/*module.exports.signup = function(req, res) {
  //console.log(req.body);
  //console.log(user);
  User
    .find({
      $and: [{
        userName: req.body.username  
        },{
        password: req.body.password  
    }]})
    .select()
    .exec(
      function(error, user) {
        if(user[0]) {
          //uporabnik že obstaja
          //console.log(user[0]);
          returnJSONResponse(res, 500, {"error": "User exists!"});
          return;
        } else if(error) {
          returnJSONResponse(res, 400, error);
          return; 
        } else {
          // uporabnik ne obstaja ... naredimo ga
          var user = new User({
            fullName: req.body.fullName,
            userName: req.body.username,
            password: req.body.password,
            playlist: {
              playlistName: req.body.username+"'s playlist",
              station: 'F',
              tag: 'F'
            }
          });
          user.save(function(error, result) {
            if(error) {
              returnJSONResponse(res, 400, error);
              return;
            } else {
              returnJSONResponse(res, 201, result);
              return;
            }
          });
        }
      }
    );
};*/

module.exports.signup = function(req, res) {
  if(!req.body.fullName || !req.body.userName || !req.body.password) {
    returnJSONResponse(res, 400 , {
      "error" : "Sumbit all fields!"
    });
    return;
  }
  
  var user = new User();
  user.fullName = req.body.fullName;
  user.userName = req.body.userName;
  user.setPassword(req.body.password);
  user.playlist = 
  {
    playlistName: req.body.userName+"'s playlist",
    station: 'F',
    tag: 'F'
  };
  user.save(function(error) {
    var token;
    if(error) {
      returnJSONResponse(res, 400, {
        "error": "Username in use!"
      });
    }
    else {
      token = user.generateJwt();
      returnJSONResponse(res, 200, {
        "token" : token
      });
    }
  });
};

