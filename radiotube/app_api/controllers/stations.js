var mongoose = require('mongoose');
var Genre = mongoose.model('Genre');
var Tag = mongoose.model('Tag');

var Playlist = mongoose.model('Playlist');

var returnJSONResponse = function(res, status, data) {
    res.status(status);
    res.json(data);
}


module.exports.getStations = function(req, res) {
    var returnData;
    if(req.params) {
        Genre
            .find()
            .exec(function(error, stations) {
                if(!stations) {
                    returnJSONResponse(res, 404, {"error" : "NOT FOUND!"});
                    return;
                } else if(error) {
                    returnJSONResponse(res, 404, error);
                    return;
                } else {
                    Tag
                        .find()
                        .exec(function(error2, stationsTags) {
                            if(!stationsTags) {
                                returnJSONResponse(res, 404, {"error" : "NOT FOUND!"});
                                return;
                            } else if(error2) {
                                returnJSONResponse(res, 404, error2);
                                return;
                            } else { 
                                //filtriranje na strežniku
                                var genre = req.params.genre;
                                var defaultPaggination = 3;
                                var genrePaggination = defaultPaggination * req.params.paggination;
                                for (var i = stations.length; i--; ) {
                                    if(stations[i].genreName == genre) {
                                        stations[i].stations = stations[i].stations.slice(0,genrePaggination);
                                    }else{
                                        stations[i].stations = stations[i].stations.slice(0,defaultPaggination);
                                    }
                                }
                                for (var i = stationsTags.length; i--; ) {
                                    if(stationsTags[i].tagName == genre) {
                                        stationsTags[i].stations = stationsTags[i].stations.slice(0,genrePaggination);
                                    }else{
                                        stationsTags[i].stations = stationsTags[i].stations.slice(0,defaultPaggination);
                                    }
                                }
                                returnData = {stations, stationsTags};
                                returnJSONResponse(res, 200, returnData);
                            }
                        });
                }
        });
    } else {
        returnJSONResponse(res, 404, {"error" : "NOT FOUND!"});
    }
};