var mongoose = require('mongoose');
var User = mongoose.model('User');
var Genre = mongoose.model('Genre');
var Playlist = mongoose.model('Playlist');
var Tag = mongoose.model('Tag');
var request = require('request');

var returnJSONResponse = function(res, status, data) {
    res.status(status);
    res.json(data);
};

module.exports.addUser = function(req, res) {
  //var user = new User(JSON.parse(req.body.data));
  var data = JSON.parse(req.body.data);
  //console.log(data.length);
  for(var x = 0; x < data.length; x++) {
    var user = new User(data[x]);
    //console.log(user);
    
    user.save(function(error, result){
    if(error) {
      returnJSONResponse(res, 400, error);
      return;
    } else {
      User
        .findById(result._id)
        .select()
        .exec(
          function(error2, data) {
            if (error2) {
              returnJSONResponse(res, 400, error);
            }
        });
      }
    });
  } 
  returnJSONResponse(res, 201, null);
};

module.exports.addGenre = function(req, res) {
  //var genre = new Genre(JSON.parse(req.body.data));
  var data = JSON.parse(req.body.data);
  //console.log(data.length);
  for(var x = 0; x < data.length; x++) { 
    var genre = new Genre(data[x]);
    
    genre.save(function(error, result){
      if(error) {
        returnJSONResponse(res, 400, error);
        return;
      } else {
        Genre
          .findById(result._id)
          .select()
          .exec(
            function(error2, data) {
              if (error2) {
                returnJSONResponse(res, 400, error);
              }
          });
      }
    });
  }
  returnJSONResponse(res, 201, null);
};

module.exports.addTag = function(req, res) {
  //var tag = new Tag(JSON.parse(req.body.data));
  var data = JSON.parse(req.body.data);
  //console.log(data.length);
  for(var x = 0; x < data.length; x++) { 
    var tag = new Tag(data[x]);
    
    tag.save(function(error, result){
      if(error) {
        returnJSONResponse(res, 400, error);
        return;
      } else {
        Genre
          .findById(result._id)
          .select()
          .exec(
            function(error2, data) {
              if (error2) {
                returnJSONResponse(res, 400, error);
              }
          });
      }
    });
  }
  returnJSONResponse(res, 201, null);
};

module.exports.deleteDb = function(req, res) {
  User
    .remove({},
      function(error) {
        if(error) {
          returnJSONResponse(res, 400, error);
          return;
        }
    });
  Genre
    .remove({},
      function(error) {
        if(error) {
          returnJSONResponse(res, 400, error);
          return;
        }
    });
    Tag
    .remove({},
      function(error) {
        if(error) {
          returnJSONResponse(res, 400, error);
          return;
        }
    });
    returnJSONResponse(res, 204, null);
};  