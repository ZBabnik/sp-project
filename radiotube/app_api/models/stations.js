var mongoose = require('mongoose');

var stationsSchema = new mongoose.Schema({
    stationName : {type: String, required: true},
    userID : {type: String, required: true},
});

var genreSchema = new mongoose.Schema({
    genreName : {type: String, required: true},
    stations : [stationsSchema]
});


mongoose.model('Genre', genreSchema, 'Genres');
//mongoose.model('Station', stationsSchema,'Stations');
