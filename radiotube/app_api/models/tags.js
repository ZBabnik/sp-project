var mongoose = require('mongoose');

var stationsSchema = new mongoose.Schema({
    stationName : {type: String, required: true},
    userID : {type: String, required: true},
});

var tagSchema = new mongoose.Schema({
    tagName : {type: String, required: true},
    stations : [stationsSchema]
});


mongoose.model('Tag', tagSchema, 'Tags');
//mongoose.model('Station', stationsSchema,'Stations');