var mongoose = require('mongoose');
var crypto = require('crypto');
var jwt = require('jsonwebtoken');

var videoSchema = new mongoose.Schema({
    videoName : {type: String, required: true},
    youtubeID : {type: String, required: true}
});

var playlistSchema = new mongoose.Schema({
    playlistName : {type: String, required: true},
    station: {type: String, required: true},
    tag: {type: String, required: true},
    videos : [videoSchema]
});

var userSchema = new mongoose.Schema({
    fullName : {type: String, required: true},
    userName : {type: String, required: true, unique: true},
    hashValue : String,
    saltValue : String,
    playlist : playlistSchema
});

userSchema.methods.setPassword = function(password) {
   this.saltValue = crypto.randomBytes(16).toString('hex');
   this.hashValue = crypto.pbkdf2Sync(password, this.saltValue, 1000, 64, 'sha512').toString('hex');
};

userSchema.methods.checkPassword = function(password) {
    var h2 = crypto.pbkdf2Sync(password, this.saltValue, 1000, 64, 'sha512').toString('hex');
    return h2 == this.hashValue;
};

userSchema.methods.generateJwt = function() {
   var expirationDate = new Date();
   expirationDate.setDate(expirationDate.getDate() + 2);
    
   return jwt.sign({
     _id: this._id,
     fullName: this.fullName,
     userName: this.userName,
     expirationDate: parseInt(expirationDate.getTime() / 1000, 10)
   }, process.env.JWT_PASS);
};
  

mongoose.model('User', userSchema, 'Users');
mongoose.model('Playlist', playlistSchema,'Playlists');