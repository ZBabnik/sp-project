(function() {
    /* global angular */
    angular.module('radiotube', ['ngRoute', 'ui.bootstrap', "youtube-embed"]);

    routes.$inject = ["$routeProvider","$locationProvider"];
    function routes($routeProvider, $locationProvider) {
       $routeProvider
         .when('/', {
             templateUrl: "/signin/signin.view.html",
             controller: "signinCtrl",
             controllerAs: "vm"
         })
         .when('/signup', {
             templateUrl: "/signup/signup.view.html",
             controller: "signupCtrl",
             controllerAs: "vm"
         })
        .when('/stations', {
            templateUrl: "/stations/stations.view.html",
            controller: "stationsCtrl",
            controllerAs: "vm"
         })
         .when('/playlist', {
            templateUrl: "/playlist/playlist.view.html",
            controller: "playlistCtrl",
            controllerAs: "vm"
         })
         .when('/profile', {
            templateUrl: "/profile/profile.view.html",
            controller: "profileCtrl",
            controllerAs: "vm"
         })
         .when('/station/:userId', {
            templateUrl: "/station/station.view.html",
            controller: "stationCtrl",
            controllerAs: "vm"
         })
         .otherwise({redirectTo: '/'});
         
         $locationProvider.html5Mode(true);
    }
      
    angular
       .module('radiotube')
       .config(['$routeProvider', '$locationProvider', routes]);
     
})();