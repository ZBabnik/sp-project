(function() {
  
  /* global angular */
  angular
    .module('radiotube')
    .controller('signupCtrl', signupCtrl);
  
  signupCtrl.$inject = ["$location", "auth"];    
  function signupCtrl($location, auth) {
    
    var vm = this;
    
    vm.signupData = {
      fullName: "",
      password: "",
      confirm: "",
      userName: ""
    };
    
    vm.sendData = function() {
      vm.formError = "";
      if(!vm.signupData.fullName || !vm.signupData.password || !vm.signupData.userName) {
        vm.formError = "Submit all fields!";
        return false;
      }
      if(vm.signupData.password != vm.signupData.confirm) {
        vm.formError = "Passwords do not match";
        return false;
      }
      else {
        vm.executeRegistration();
      }
    };
    
    vm.executeRegistration = function() {
      vm.formError = "";
      auth
        .signup(vm.signupData)
        .then(
          function success() {
            $location.path("/");
          },
          function error(error) {
            console.log(error);
            vm.formError = error.data.error;
          }
        );
    };
  }
     
})();