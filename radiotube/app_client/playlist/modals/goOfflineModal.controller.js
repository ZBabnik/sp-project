(function() {
  
  /* global angular */
  function goOfflineModal($uibModalInstance, auth, playlist) {
    
    var vm = this;
    
    vm.modalWindow = {
      close: function() {
        $uibModalInstance.close();
      },
      close2: function(data) {
        $uibModalInstance.close(data);
      }
    };
    
    vm.sendData = function() {
      vm.formError = "";
      playlist.goOffline((auth.currentUser()).userId)
        .then(
          function success(res) {
            vm.modalWindow.close2("changeIsOnline");
          },
          function error(error) {
            vm.formError = "Server error, try again later!";
          });
      };
  }
  goOfflineModal.$inject = ["$uibModalInstance", "auth", "playlist"];
  
  angular
    .module('radiotube')
    .controller('goOfflineModal', goOfflineModal);
})();