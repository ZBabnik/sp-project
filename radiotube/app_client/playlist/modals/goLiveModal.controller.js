(function() {
  
  /* global angular */
  function goLiveModal($uibModalInstance, auth, playlist) {
    
    var vm = this;
    
    vm.data = {
      genre: "",
      tag: ""
    };
    
    vm.modalWindow = {
      close: function() {
        $uibModalInstance.close();
      },
      close2: function(data) {
        $uibModalInstance.close(data);
      }
    };
    
    vm.sendData = function() {
      vm.formError = "";
      if(!vm.data.genre || !vm.data.tag) {
        vm.formError = "Choose both values!";
      }
      else {
        playlist.goLive((auth.currentUser()).userId, {
          stationGenre: vm.data.genre,
          stationTag: vm.data.tag
        }).then(
          function success(res) {
            vm.modalWindow.close2(res.data);
          },
          function error(error) {
            vm.formError = "Server error, try again later!";
          });
      }
    };
  }
  goLiveModal.$inject = ["$uibModalInstance", "auth", "playlist"];
  
  angular
    .module('radiotube')
    .controller('goLiveModal', goLiveModal);
})();