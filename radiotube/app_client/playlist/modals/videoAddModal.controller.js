(function() {
  
  /* global angular */
  videoAddModal.$inject = ["$uibModalInstance", "auth", "playlist"];
  function videoAddModal($uibModalInstance, auth, playlist) {
    
    var vm = this;

    vm.data = {
      videoLink: ""
    };

    vm.modalWindow = {
      close: function() {
        $uibModalInstance.close();
      },
      close2: function(res) {
        $uibModalInstance.close(res);
      }
    };
    
    vm.sendData = function() {
      vm.formError = "";
      if(!vm.data.videoLink || !(/https:\/\/www.youtube.com\/watch\?v=/.test(vm.data.videoLink))) {
        vm.formError = "Enter a correct link!";
      }
      else {
        playlist.addVideo((auth.currentUser()).userId, {
          videoName: "A bo kdo youtubeAPI delov?",
          youtubeID: vm.data.videoLink
        }).then(
          function success(res) {
            vm.modalWindow.close2(res.data);
          },
          function error(error) {
            vm.formError = error.data.error;
          });
      }
    };
  }
  
  angular
    .module('radiotube')
    .controller('videoAddModal', videoAddModal);
})();