(function() {
  
  /* global angular */
  function renameModal($uibModalInstance, auth, playlist) {
    
    var vm = this;
    
    vm.modalWindow = {
      close: function() {
        $uibModalInstance.close();
      },
      close2: function(data) {
        $uibModalInstance.close(data);
      }
    };
    
    vm.data = {
      playlistName: ""
    };
    
    vm.sendData = function() {
      vm.formError = "";
      if(!vm.data.playlistName) {
        vm.formError = "Enter a valid name!";
      }
      else {
        playlist.renamePlaylist((auth.currentUser()).userId, {
          playlistName: vm.data.playlistName
        }).then(
          function success(res) {
            console.log(res.data.playlist);
            vm.modalWindow.close2(res.data.playlist);
          },
          function error(error) {
            vm.formError = "Server error, try again later!";
          });
      }
    };
  }
  renameModal.$inject = ["$uibModalInstance", "auth", "playlist"];
  
  angular
    .module('radiotube')
    .controller('renameModal', renameModal);
})();