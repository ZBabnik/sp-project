(function() {
    
  /* global angular */
  angular
    .module('radiotube')
    .controller('playlistCtrl', playlistCtrl);
    
  playlistCtrl.$inject = ["$location", 'playlist', "auth", '$uibModal', 'log'];  
  function playlistCtrl($location, playlist, auth, $uibModal, log) {
    
    var vm = this;
    
    vm.userId = (auth.currentUser()).userId;
    
    playlist.returnPlaylist((auth.currentUser()).userId)
      .then(
        function success(data) {
          vm.playlistName = data.data.playlistName;
          vm.videos = data.data.videos;

          if(data.data.station != "F" && data.data.tag != "F") {
            vm.isOnline = true;
          }
          else {
            vm.isOnline = false;
          }
        },
        function error(error) {
          console.log(error);
          vm.playlistError = "Failed to fetch playlist info!"; 
        }
    );
    
    /* for user logging */
    vm.locationLog = {
      location: "/playlists",
      userID: (auth.currentUser()).userId
    };
    
    log.trackUser(vm.locationLog)
      .then(
        function success(data) {
          // do nothing
        },
        function error(error) {
          // do noting
        }
      );
    /* for user logging */
    
    vm.visitStation = function(userId) {
      $location.path('/station/'+userId);
    };
    
    vm.removeVideo = function(videoId) {
      vm.playlistError = "";
      
      playlist.deleteVideo((auth.currentUser()).userId, videoId) 
        .then(
          function success(res) {
            var myEl = angular.element(document.getElementById(videoId));
            myEl.remove();  
            
            for(var i = 0; i < vm.videos.length; i++) {
              if(vm.videos[i]._id == videoId) {
                vm.videos.splice(i, 1);
                break;
              }
            }
          },
          function error(error) {
            vm.playlistError = "Server error, try again later!";
          }
        );
    };
    
    vm.showVideoAddModal = function() {
      vm.playlistError = "";
      
      var modalInstance = $uibModal.open({
        templateUrl: "/playlist/modals/videoAddModal.view.html",
        controller: "videoAddModal",
        controllerAs: "vm"
      });  
      
      modalInstance.result.then(function(data) {
         if (typeof data != 'undefined')
            vm.videos.push(data);
      }); 
    };
    
    vm.showRenameModal = function() {
      vm.playlistError = "";
      
      var modalInstance = $uibModal.open({
        templateUrl: "/playlist/modals/renameModal.view.html",
        controller: "renameModal",
        controllerAs: "vm"
      }); 
      
      modalInstance.result.then(function(data) {
        vm.playlistName = data.playlistName;
      }); 
    };
    
    vm.showGoLiveModal = function() {
      vm.playlistError = "";
      
      if(!vm.videos || vm.videos == null || vm.videos.length == 0) {
        vm.playlistError = "Your playlist is empty, add atleast one video to golive!";
        return;
      }
      
      var modalInstance = $uibModal.open({
        templateUrl: "/playlist/modals/goLiveModal.view.html",
        controller: "goLiveModal",
        controllerAs: "vm"
      });  
      
      modalInstance.result.then(function(data) {
        if(data.station != "F" && data.tag != "F")
        vm.isOnline = true;
      });
    };
    
    vm.showGoOfflineModal = function() {
      vm.playlistError = "";
      
      var modalInstance = $uibModal.open({
        templateUrl: "/playlist/modals/goOfflineModal.view.html",
        controller: "goOfflineModal",
        controllerAs: "vm"
      });
      
      modalInstance.result.then(function(data) {
        if(data == "changeIsOnline")
          vm.isOnline = false;
      });
    };
  }
})();