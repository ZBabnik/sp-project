(function() {
  /* global angular */
  var header = function() {
    return {
      restrict: 'EA',
      templateUrl: "/common/directive/head.template.html",
      controller: "headCtrl",
      controllerAs: "navvm"
    };
  };
  
  angular
    .module('radiotube')
    .directive('header', header);
})();