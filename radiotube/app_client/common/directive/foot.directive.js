(function() {
  /* global angular */
  
  var noga = function() {
    return {
      restrict: 'EA',
      templateUrl: "/common/directive/foot.template.html"
    };
  };
  
  angular
    .module('radiotube')
    .directive('noga', noga);
})();