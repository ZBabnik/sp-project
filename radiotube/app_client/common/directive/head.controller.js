(function() {
    
    /* global angular*/
    headCtrl.$inject = ["auth", "$location"];
    function headCtrl(auth, $location) {
        var navvm = this;
        
        navvm.user = auth.currentUser().userName;
        
        navvm.signout = function() {
            auth.signout();
            $location.path("/");
        };
    }
    
    angular
        .module('radiotube')
        .controller('headCtrl', headCtrl);
})();