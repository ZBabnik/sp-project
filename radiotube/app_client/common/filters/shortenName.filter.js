(function() {
  
  /* global angular */
  var shortenString = function() {
    return function(videoName) {
      return videoName.substring(0, 15)+"...";
    };
  };
  
  angular
    .module('radiotube')
    .filter('shortenString', shortenString);
})();