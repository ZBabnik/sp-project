(function() {
  /* global angular */
  angular
    .module('radiotube')
    .controller('signinCtrl', signinCtrl);
    
  signinCtrl.$inject = ["$location", "auth"];    
  function signinCtrl($location, auth) {
    
    var vm = this;
    
    vm.signinData = {
      userName: "",
      password: ""
    };

    vm.sendData = function() {
      vm.formError = "";
      if(!vm.signinData.userName || !vm.signinData.password) {
        vm.formError = "Submit all fields!";
        return false;
      }
      else {
        vm.executeSignin();
      }
    };
    
    vm.executeSignin = function() {
      vm.formError = "";
      auth
        .signin(vm.signinData)
        .then(
          function success() {
            $location.path('/stations');
          }, 
          function error(error) {
            console.log(error);
            vm.formError = error.data.error;
          }
        );
    };
  } 
     
})();