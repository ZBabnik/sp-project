(function() {
    
  /* global angular */
  angular
    .module('radiotube')
    .controller('profileCtrl', profileCtrl);
    
  profileCtrl.$inject = ['auth', 'radiotubeData', '$location', '$window', 'log', '$uibModal'];  
  function profileCtrl(auth, radiotubeData, $location, $window, log, $uibModal) {
    
    var vm = this;
    
    vm.fullNamePlace = auth.currentUser().fullName;
    vm.userNamePlace = auth.currentUser().userName;
    
    vm.userName = vm.userNamePlace;
    vm.fullName = vm.fullNamePlace;
    
    /* for user logging */
    vm.locationLog = {
      location: "/profile",
      userID: (auth.currentUser()).userId
    };
    
    log.trackUser(vm.locationLog)
      .then(
        function success(data) {
          // do nothing
        },
        function error(error) {
          // do noting
        }
      );
    /* for user logging */
    
    vm.submit = function(){
      if((vm.fullName == undefined || vm.fullName == "") && (vm.userName == undefined || vm.userName == "")) {
        vm.profileError = "Nothing to update!";
        return;
      }
      var dataUser = auth.currentUser();

      var fullName, userName;
      if(vm.fullName != undefined)
        fullName = vm.fullName;
      else
        fullName = dataUser.fullName;
        
      if(vm.userName != undefined)
        userName = vm.userName;
      else  
        userName = dataUser.userName;
        
        console.log(userName, fullName);
        
     //console.log("A");  
      //console.log("sent " + dataUser.userId + " " + fullName + " " + userName + " to radiotubeData");
      radiotubeData.updateProfile(dataUser.userId, fullName, userName)
         .then(
          function success(res) {
             vm.changeUserModal();
          },
          function error(error) {
            console.log("A");
            console.log(error);
            vm.profileError = error.data.error;
          }
        );
    };
    
    vm.changeUserModal = function() {
      var modalInstance = $uibModal.open({
        templateUrl: "/profile/modals/changeUserModal.view.html",
        controller: "changeUserModal",
        controllerAs: "vm"
      });  
      
      modalInstance.result.then(function() {
          auth.signout();
          $location.path("/");
      }, function() {
          auth.signout();
          $location.path("/");
      }); 
    };
  } 
       
})();