(function() {
  
  /* global angular */
  function changeUserModal($uibModalInstance, auth) {
    
    var vm = this;
    
    vm.modalWindow = {
      close: function() {
        $uibModalInstance.close();
      },
      close2: function(res) {
        $uibModalInstance.close(res);
      }
    };
    
    
  }
  changeUserModal.$inject = ["$uibModalInstance", "auth"];
  
  angular
    .module('radiotube')
    .controller('changeUserModal', changeUserModal);
})();