(function(){
    
    /* global angular */
    
    var playlist = function($http, auth) {
        
        var returnPlaylist = function(userId) {
            return $http.get('/api/v1/playlists/' + userId, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        var updatePlaylist = function(userId){
            return $http.put('/api/v1/playlists/' + userId, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });  
        };
        
        var addVideo = function(userId, data) {
            return $http.post('/api/v1/playlists/' + userId, data, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        var renamePlaylist = function(userId, data) {
           return $http.put('/api/v1/playlists/' + userId, data, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            }); 
        };
        
        var deleteVideo = function(userId, videoId) {
            return $http.delete('/api/v1/playlists/' + userId + '/' + videoId, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        var goLive = function(userId, data) {
            return $http.post('/api/v1/playlistsLive/' + userId, data, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        var goOffline = function(userId) {
            return $http.delete('/api/v1/playlistsLive/' + userId, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        return {
            returnPlaylist: returnPlaylist,
            updatePlaylist: updatePlaylist,
            addVideo: addVideo,
            renamePlaylist: renamePlaylist,
            deleteVideo: deleteVideo,
            goLive: goLive,
            goOffline: goOffline
        };
    };
    
    playlist.$inject = ['$http', 'auth'];
    
    angular
        .module('radiotube')
        .service('playlist', playlist);
})();