(function () {
  /* global angular */
  authentication.$inject = ['$window', "$http"];
  function authentication($window, $http) {
    
    var storeToken = function(token) {
      $window.localStorage['radiotube-token'] = token;
    };
    
    var returnToken = function() {
       return $window.localStorage['radiotube-token'];
    };
    
    var signup = function(user) {
      return $http
                .post('/api/v1/signup', user)
                .then(function success(res) {
                  storeToken(res.data.token);
                });
    };
    
    var signin = function(user) {
      return $http
                .post('/api/v1/signin', user)
                .then(function success(res) {
                  storeToken(res.data.token);
                });
    };
    
    var signout = function() {
      $window.localStorage.removeItem('radiotube-token');
    };
    
    var isSignedin = function() {
      var token = returnToken();
      if(token) {
        var tokenContent = JSON.parse($window.atob(token.split('.')[1]));
        return tokenContent.expirationDate > Date.now() / 1000;
      } 
      else {
        return false;
      }
    };
    
    var currentUser = function() {
      if(isSignedin()) {
        var token = returnToken();
        var tokenContent = JSON.parse($window.atob(token.split('.')[1]));
        return {
          userId: tokenContent._id,
          fullName: tokenContent.fullName,
          userName: tokenContent.userName
        };
      }
    };
    
    return {
       storeToken: storeToken,
       returnToken: returnToken,
       signup: signup,
       signin: signin,
       signout: signout,
       isSignedin: isSignedin,
       currentUser: currentUser
     };
   }
    
   angular
     .module('radiotube')
     .service('auth', authentication);
})();