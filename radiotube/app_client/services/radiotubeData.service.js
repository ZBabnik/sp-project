(function(){
    /* global angular */
    
   
    var radiotubeData = function($http, auth){
 /*       var addVideo = function(userId){
            return $http.post('/api/v1/playlists/' + userId);  
        };
        
        var removeVideo = function(userId){
            return $http.delete('/api/v1/playlists/' + userId);   
        };
        
        var returnStation = function(stationId){
            return $http.get('/api/v1/playlists/' + stationId);   
        };
*/        
        var returnStations = function(genre, paggination){
            var url = '/api/v1/stations/'+ genre + '/'+ paggination;
            console.log(url)
            return $http.get(url, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
 
        };
        
        var updateProfile = function(userId, fullName, userName){
            console.log(fullName, userName);
            var data = {
                fullName: fullName, 
                userName: userName,
            };
            return $http.put('/api/v1/profile/'+ userId, data, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
            });
        };
        
        return {
            updateProfile: updateProfile,
 /*         addVideo: addVideo,
            addVideo: addVideo,
            removeVideo: removeVideo,
            returnStation: returnStation,
*/          returnStations: returnStations
        };
    };
    
    radiotubeData.$inject = ['$http', 'auth'];
     
    angular
        .module('radiotube')
        .service('radiotubeData', radiotubeData);
})();