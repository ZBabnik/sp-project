(function() {
    
  /* global angular */
  logService.$inject = ["$http", "auth"];
  function logService($http, auth) {
       
    var trackUser = function(data) {
      return $http.post('/api/v1/logs', data, {
                headers: {
                  Authorization: 'Bearer ' + auth.returnToken()
                }
              });
    };
    
    return {
      trackUser: trackUser
    };
  }
    
  angular
    .module('radiotube')
    .service('log', logService);
    
})();