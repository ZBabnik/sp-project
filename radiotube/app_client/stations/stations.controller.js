(function() {
    
  /* global angular */
  angular
    .module('radiotube')
    .controller('stationsCtrl', stationsCtrl);
    
  stationsCtrl.$inject = ['radiotubeData', 'auth', 'log'];  
  function stationsCtrl(radiotubeData, auth, log) {
    
    var vm = this;
    vm.showDetails = "";
    vm.selectOptions = [1, 2, 3, 5, 10, 50, 100];
    vm.paggination = 1;
    vm.numPlaylists = vm.paggination;
    vm.noMore = false;
     
    radiotubeData.returnStations("all", vm.paggination)
      .then(
        function success(data) {
          vm.stations = data.data.stations;
          vm.stationsTags = data.data.stationsTags;
  //        console.log(vm.stations);
        },
        function error(error) {
          console.log(error);
          vm.stationsError = "Failed to fetch stations info!"; 
        }
    );
    
    /* for user logging */
    vm.locationLog = {
      location: "/stations",
      userID: (auth.currentUser()).userId
    };
    
    log.trackUser(vm.locationLog)
      .then(
        function success(data) {
          // do nothing
        },
        function error(error) {
          // do noting
        }
      );
    /* for user logging */
    
    var updatePaggination = function(genOrTag, genre) {
      vm.paggination += 1;
      
      // pridobimo podatke
      radiotubeData.returnStations(genre, vm.paggination)
      .then(
        function success(data) {
          vm.stations = data.data.stations;
          vm.stationsTags = data.data.stationsTags;
          
          //console.log(vm.stations);
        },
        function error(error) {
          console.log(error);
          vm.stationsError = "Failed to fetch stations info!"; 
        }
      );
      if(genOrTag) {
        var result  = vm.stations.filter(function(o){ return o.genreName == genre;});
      }else{
        var result  = vm.stationsTags.filter(function(o){ return o.tagName == genre;});
      }
     
   //   console.log("resut:" + result[0].stations.length+ " num: "+ vm.numPlaylists)
      
      if(vm.numPlaylists < result[0].stations.length || vm.numPlaylists == 0){
        vm.noMore = true;        
      }
      else {
        vm.numPlaylists = result[0].stations.length; 
        if(vm.numPlaylists == 0)
          vm.noMore = true;
      }
    };
    
    // zanri
    vm.updateGenres = function(genre){
      updatePaggination(true, genre)
    }
    
    // tagi
    vm.updateTags = function(genre){
      updatePaggination(false, genre)
    }
    
    vm.buttonToggle = function(id) {
      vm.paggination = 1;
      vm.numPlaylists = 0;
      vm.noMore = false;
      if(id == vm.showDetails)
        vm.showDetails = "";
      else
        vm.showDetails = id;
    };
    
  }
  
})();