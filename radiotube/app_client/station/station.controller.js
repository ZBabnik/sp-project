(function() {
    
  /* global angular */
  angular
    .module('radiotube')
    .controller('stationCtrl', stationCtrl);
    
  stationCtrl.$inject = ['$scope','$routeParams', 'playlist', 'auth', 'log'];  
  function stationCtrl($scope, $routeParams, playlist, auth, log) {
    
    var vm = this;
    
    vm.headerContent = auth.currentUser().userName;
    
    vm.toggleEnabled = false;
    
    playlist.returnPlaylist($routeParams.userId)
      .then(
        function success(data) {
          vm.stationName = data.data.playlistName;
          vm.videos = data.data.videos;
          $scope.videoPlayer = vm.videos[0].youtubeID;
          
          $scope.playerVars = {
            controls: 0,
            autoplay: 1
          };
          
          console.log(vm.videos[0]._id);
          vm.i = 1;
        },
        function error(error) {
          console.log(error);
          vm.playlistError = "Failed to fetch playlist info!"; 
        }
    );
    
    /* for user logging */
    vm.locationLog = {
      location: "/station/" + $routeParams.userId,
      userID: (auth.currentUser()).userId
    };
    
    log.trackUser(vm.locationLog)
      .then(
        function success(data) {
          // do nothing
        },
        function error(error) {
          // do noting
        }
      );
    /* for user logging */
    
    
    /**
     * Returns a random integer between min (inclusive) and max (inclusive)
     * Using Math.round() will give you a non-uniform distribution!
     */
    vm.getRandomInt = function(min, max) {
        return Math.floor(Math.random() * (max - min + 1)) + min;
    };
    
    
    $scope.$on('youtube.player.ended', function ($event, player) {
        if(vm.toggleEnabled) {
          vm.i = vm.getRandomInt(0, (vm.videos.length - 1));
        }
        else {
          if(vm.i >= vm.videos.length)
              vm.i = 0;
        }
        console.log("playing "+vm.i);
        $scope.videoPlayer = vm.videos[vm.i].youtubeID;
        vm.i += 1;
        player.playVideo();
     });
   
   $scope.$on('youtube.player.error', function ($event, player) {
        if(vm.toggleEnabled) {
          vm.i = vm.getRandomInt(0, (vm.videos.length - 1));
        }
        else {
          if(vm.i >= vm.videos.length)
              vm.i = 0;
        }
        console.log("playing "+vm.i);
        $scope.videoPlayer = vm.videos[vm.i].youtubeID;
        vm.i += 1;
        player.playVideo();
     });
   
     vm.turnOnToggle = function() {
       console.log("on");
       vm.toggleEnabled = true;
     };
     
     vm.turnOffToggle = function() {
       console.log("off");
       vm.toggleEnabled = false;
     };
  }
  
})();