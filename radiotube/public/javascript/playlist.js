/*
$(document).ready(function() {
	
	// Passes video name to modal
	$("#remove-modal").on('show.bs.modal', function(e) {
        var modalText = "Are you sure you want to remove "+e.relatedTarget.dataset.videoname+" from your playlist?";
        var videoID = e.relatedTarget.dataset.dbid;
        //console.log(videoID);
        //console.log(e.currentTarget.getElementsByTagName("form")[0].attr("action"));
        var actionAttr = ($("#videoDelete-form").attr("action")) +"/"+ videoID;
        $("#videoDelete-form").prop("action", actionAttr);
        e.currentTarget.getElementsByTagName("p")[0].innerHTML = modalText;
        
        e.currentTarget.dataset.videoname = e.relatedTarget.dataset.videoname;
	});
	
	// Removes video entry from page
	$("#removeVideo").on("click", function(e) {
	    var videoName = document.getElementById("remove-modal").dataset.videoname;
	    var videoEntry = document.getElementById(videoName);
	    videoEntry.parentNode.removeChild(videoEntry);
	});
	
	// Checks entered YouTube URL / adds entry
	$("#addVideo").on("click", function(e) {
	    var videoName = $("#playlist-add-input").val();
	    if(videoName != "" && /https:\/\/www.youtube.com\/watch\?v=/.test(videoName)) {
	        
	       document.getElementById("playlist-list-group").innerHTML +=
	            "<div id='This is not a real song!' class='col-xs-12 list-group-item'> \
	                <div> This is not a real song! \
	                    <button data-toggle='modal' data-target='#remove-modal' data-videoName='This is not a real song!' class='btn btn-danger removeVideoButton'> \
	                        <span class='glyphicon glyphicon-trash'></span>\
	                    </button> \
	                </div>\
	              </div>";
	        
	        $("#playlist-added-success").show();
	        setTimeout(function() {
	            $("#playlist-added-success").hide();
	        }, 2000);
	    } else {
	        $("#playlist-added-fail").show();
	        setTimeout(function() {
	            $("#playlist-added-fail").hide();
	        }, 2000);
	    }
	});
	
	//Renames the current playlist
	$("#renamePlaylist").on("click", function(e) {
	    var videoName = $("#playlist-rename-input").val();
	    if(videoName != "") {
	        document.getElementById("playlist-name").innerHTML = videoName;
	        
	        $("#playlist-rename-success").show();
	        setTimeout(function() {
	            $("#playlist-rename-success").hide();
	        }, 2000);
	    } else {
	        $("#playlist-rename-fail").show();
	        setTimeout(function() {
	            $("#playlist-rename-fail").hide();
	        }, 2000);
	    }
	});
});

*/