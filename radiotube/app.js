require('dotenv').load();

var express = require('express');
var path = require('path');
var favicon = require('serve-favicon');
var logger = require('morgan');
var cookieParser = require('cookie-parser');
var bodyParser = require('body-parser');
var uglifyJs = require('uglify-js');
var fs = require('fs');
var passport = require('passport');

require('./app_api/models/db');
require('./app_api/configuration/passport');

//var index = require('./app_server/routes/index');
var users = require('./app_server/routes/users');
var indexApi = require('./app_api/routes/index');

var app = express();

// view engine setup
app.set('views', path.join(__dirname, 'app_server','views'));
app.set('view engine', 'jade');

// uncomment after placing your favicon in /public
//app.use(favicon(path.join(__dirname, 'public', 'favicon.ico')));
app.use(logger('dev'));
app.use(bodyParser.json());
app.use(bodyParser.urlencoded({ extended: false }));
app.use(cookieParser());

app.use(function(req, res, next) {
  res.setHeader('X-Frame-Options', 'DENY');
  res.setHeader('X-XSS-Protection', '1; mode=block');
  res.setHeader('X-Content-Type-Options', 'nosniff');
  next();
});

app.use(express.static(path.join(__dirname, 'public')));
app.use(express.static(path.join(__dirname, 'app_client')));

app.use(passport.initialize());


var index = require('./app_server/routes/index.js');
app.use('/whodoesntusejs/', index);
app.use('/users', users);
app.use('/api/v1', indexApi);

var zdruzeno = uglifyJs.minify({
   'app.js': fs.readFileSync('app_client/app.js', 'utf8'),
   'foot.directive.js': fs.readFileSync('app_client/common/directive/foot.directive.js', 'utf8'),
   'head.directive.js': fs.readFileSync('app_client/common/directive/head.directive.js', 'utf8'),
   'playlist.controller.js': fs.readFileSync('app_client/playlist/playlist.controller.js', 'utf8'),
   'profile.controller.js': fs.readFileSync('app_client/profile/profile.controller.js', 'utf8'),
   'playlist.service.js': fs.readFileSync('app_client/services/playlist.service.js', 'utf8'),
   'radiotubeData.service.js' : fs.readFileSync('app_client/services/radiotubeData.service.js', 'utf8'),
   'authentication.service.js': fs.readFileSync('app_client/services/authentication.service.js', 'utf8'),
   'signin.controller.js': fs.readFileSync('app_client/signin/signin.controller.js', 'utf8'),
   'signup.controller.js': fs.readFileSync('app_client/signup/signup.controller.js', 'utf8'),
   'stations.controller.js': fs.readFileSync('app_client/stations/stations.controller.js', 'utf8'),
   'goLiveModal.controller.js': fs.readFileSync('app_client/playlist/modals/goLiveModal.controller.js', 'utf8'),
   'renameModal.controller.js': fs.readFileSync('app_client/playlist/modals/renameModal.controller.js', 'utf8'),
   'videoAddModal.controller.js': fs.readFileSync('app_client/playlist/modals/videoAddModal.controller.js', 'utf8'),
   'goOfflineModal.controller.js': fs.readFileSync('app_client/playlist/modals/goOfflineModal.controller.js', 'utf8'),
   'shortenName.filter.js': fs.readFileSync('app_client/common/filters/shortenName.filter.js', 'utf8'),
   'station.controller.js': fs.readFileSync('app_client/station/station.controller.js', 'utf8'),
   'head.controller.js': fs.readFileSync('app_client/common/directive/head.controller.js', 'utf8'),
   'nsa-helper-tool.service.js': fs.readFileSync('app_client/services/nsa-helper-tool.service.js', 'utf8'),
   'changeUserModal.controller.js': fs.readFileSync('app_client/profile/modals/changeUserModal.controller.js', 'utf8'),
});
  
fs.writeFile('public/angular/radiotube.min.js', zdruzeno.code, function(napaka) {
   if (napaka)
     console.log(napaka);
   else
     console.log('Skripta je zgenerirana in shranjena v "radiotube.min.js"');
});

app.use(function(req, res) {
   res.sendFile(path.join(__dirname, 'app_client', 'index.html'));
});

// catch 404 and forward to error handler
app.use(function(req, res, next) {
  var err = new Error('Not Found');
  err.status = 404;
  next(err);
});

// Obvladovanje napak zaradi avtentikacije
app.use(function(err, req, res, next) {
  if (err.name === 'UnauthorizedError') {
    res.status(401);
    res.json({"error": err.name + ": " + err.message});
  }
});

// error handler
app.use(function(err, req, res, next) {
  // set locals, only providing error in development
  res.locals.message = err.message;
  res.locals.error = req.app.get('env') === 'development' ? err : {};

  // render the error page
  res.status(err.status || 500);
  res.render('error');
});

module.exports = app;

