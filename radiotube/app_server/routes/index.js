var express = require('express');
var router = express.Router();
var ctrlMain = require("../controllers/main.js");
var ctrlOther = require("../controllers/other.js");

/* GET home page. */
router.get('/', ctrlMain.signin);
//router.get('/', ctrlOther.angularApp);
router.get('/playlist', ctrlMain.playlist);
router.post('/playlist', ctrlMain.addVideo);
router.post('/playlistRename', ctrlMain.renamePlaylist);
router.get('/playlist', ctrlMain.removeVideo);
router.post('/playlistLive', ctrlMain.goLive);
router.post('/playlistOffline', ctrlMain.goOffline);
router.get('/signup', ctrlMain.signup);
router.post('/signup', ctrlMain.signupConfirm);
router.post('/signin', ctrlMain.signinConfirm);
router.get('/profile', ctrlMain.profile);
router.get('/stations', ctrlMain.stations);
router.get("/station", ctrlMain.station);



router.get("/db", ctrlMain.db);
router.get("/db/delete", ctrlMain.dbdelete);
router.post("/db/users", ctrlMain.dbUsers);
router.post("/db/genres", ctrlMain.dbGenres);
router.post("/db/tags", ctrlMain.dbTags);

module.exports = router;
