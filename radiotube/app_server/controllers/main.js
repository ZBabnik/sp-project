var request = require('request');
var apiParametri = {
   streznik: "http://localhost:" + process.env.PORT
};

if (process.env.NODE_ENV === 'production') {
  apiParametri.streznik = "https://radiotube-sp.herokuapp.com";
}

module.exports.index = function(req, res) {
  res.render('index', { title: 'RadioTube' });
};

module.exports.addVideo = function(req, res) {
  var reqUser, path, reqParams;
  path = '/api/v1/playlists/';
  reqUser = req.params.userID;
  reqParams = {
    url: apiParametri.streznik + path + reqUser,
    method: 'POST',
    json: {
      videoName: "Thundercat - 'Show You The Way (feat. Michael McDonald & Kenny Loggins)' (Official Video)",
      youtubeID: "https://www.youtube.com/watch?v=Z-zdIGxOJ4M"
    }
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 201) {
        //console.log(response.body[0]._id);
        //console.log(response);
        console.log(data);
        res.redirect('/playlist/'+reqUser);
      } else {
        //errror
        res.redirect('/playlist/'+reqUser);
      }   
    }
  );
};

module.exports.renamePlaylist = function(req, res) {
  var reqUser, path, reqParams;
  path = '/api/v1/playlists/';
  reqUser = req.params.userID;
  reqParams = {
    url: apiParametri.streznik + path + reqUser,
    method: 'PUT',
    json: {
      playlistName : req.body.playlistName
    }
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 200) {
        //console.log(response.body[0]._id);
        //console.log(response);
        res.redirect('/playlist/'+reqUser);
      } else {
        //errror
        res.redirect('/playlist/'+reqUser);
      }   
    }
  );
}

module.exports.removeVideo = function(req, res) {
  var reqUser, path, reqParams, reqVideo;
  path = '/api/v1/playlists/';
  reqUser = req.params.userID;
  reqVideo = req.params.videoID;
  reqParams = {
    url: apiParametri.streznik + path + reqUser + "/" + reqVideo,
    method: 'DELETE',
    json: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 204) {
        //console.log(response.body[0]._id);
        //console.log(response);
        res.redirect('/playlist/'+reqUser);
      } else {
        //errror
        res.redirect('/playlist/'+reqUser);
      }   
    }
  );
}

var renderPlaylist = function(req, res, data) {
  res.render('playlist', {
    playlistName: {},
    videos: {},
    user: ""
  });
};

module.exports.playlist = function(req, res) {
  var reqParams, path, userID;
  userID = req.params.userID;
  path = '/api/v1/playlists/';
  reqParams = {
    url: apiParametri.streznik + path + userID,
    method: 'GET',
    json: {},
    qs: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      //console.log(data);
      renderPlaylist(req, res, data);     
    }
  );
};

module.exports.signin = function(req, res) {
  res.render('signin', {});
};

module.exports.signinConfirm = function(req, res) {
  
  console.log(req.body);
  var reqData, path, reqParams;
  path = '/api/v1/signin';
  reqParams = {
    url: apiParametri.streznik + path,
    method: 'POST',
    json: {
      userName: req.body.userName, 
      password: req.body.password
    }
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 200) {
        //console.log(response.body[0]._id);
        //console.log(response);
        res.redirect('/whodoesntusejs/stations');
      } else if(response.statusCode == 500 && response.body.error == 'H') {
        // Izpis napačn geslo/username
        res.redirect('/whodoesntusejs/');
      } else {
        console.log(data);
        res.redirect('/whodoesntusejs/');
      }   
    }
  );
};

module.exports.signup = function(req, res) {
  res.render('signup', { title: 'RadioTube' });
};

module.exports.signupConfirm = function(req, res) {
  //console.log(req.body);
  var reqData, path, reqParams;
  path = '/api/v1/signup';
  //console.log(req.body);
  reqParams = {
    url: apiParametri.streznik + path,
    method: 'POST',
    json: {
      fullName: req.body.name,
      username: req.body.username, 
      password: req.body.password
    }
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 201) {
        res.redirect('/whodoesntusejs/');
        return;
      } else if(response.statusCode == 500) {
        // Izpis napačn geslo/username
        res.redirect('/whodoesntusejs/signup');
        return;
      } else {
        res.redirect('/whodoesntusejs/signup');
        return;
      }   
    }
  );
}

var renderProfile = function(req, res, data) {
res.render('profile', { 
    title: 'Profile',
    fullName: data.fullName,
    userName: data.userName,
    password: data.password,
    // DA NEBI TEGA ZBRISOV...
    user: req.params.userID
  });
};

module.exports.profile = function(req, res) {
  var reqParams, path, userID;
  userID = req.params.userID;
  path = '/api/v1/users/';
  reqParams = {
    url: apiParametri.streznik + path + userID,
    method: 'GET',
    json: {},
    qs: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      console.log(data);
      renderProfile(req, res, data);     
    }
  );
};

var renderStations = function(req, res, data) {
  //console.log(data.stations);
  //console.log(data.stationsTags)
  res.render('stations', {
    zanrList: {},
    tagsList: {},
    user: ""
  });
};

module.exports.stations = function(req, res) {
  var reqParams, path;
  path = '/api/v1/stations/all/1';
  reqParams = {
    url: apiParametri.streznik + path,
    method: 'GET',
    json: {},
    qs: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      //console.log(data);
      renderStations(req, res, data);     
    }
  );
};

module.exports.db = function(req, res) {
  res.render('db', {});
};

module.exports.dbUsers = function(req, res) {
  var requestParams = {
    url: apiParametri.streznik + "/api/v1/dbUsers",
    method: 'POST',
    json: {
      data: req.body.data.replace(/(\r?\n|\r|\n)/gm, "").replace(/ /g, "")
    }
  };
  request(
    requestParams,
    function(error, res2, data) {
      if(res2.statusCode == 201) {
        console.log("User added");
        res.redirect('/db');
      } else {
        console.log("Error adding user");
        res.redirect('/db');
      }
    }
  );
};

module.exports.dbGenres = function(req, res) {
  var requestParams = {
    url: apiParametri.streznik + "/api/v1/dbGenres",
    method: 'POST',
    json: {
      data: req.body.data.replace(/(\r?\n|\r|\n)/gm, "").replace(/ /g, "")
    }
  };
 request(
    requestParams,
    function(error, res2, data) {
      if(res2.statusCode == 201) {
        console.log("Genre added");
        res.redirect('/db');
      } else {
        console.log("Error adding genre");
        res.redirect('/db');
      }
    }
  );
};

module.exports.dbTags = function(req, res) {
  var requestParams = {
    url: apiParametri.streznik + "/api/v1/dbTags",
    method: 'POST',
    json: {
      data: req.body.data.replace(/(\r?\n|\r|\n)/gm, "").replace(/ /g, "")
    }
  };
 request(
    requestParams,
    function(error, res2, data) {
      if(res2.statusCode == 201) {
        console.log("Tag added");
        res.redirect('/db');
      } else {
        console.log("Error adding tag");
        res.redirect('/db');
      }
    }
  );
};

module.exports.dbdelete = function(req, res) {
  var requestParams = {
    url: apiParametri.streznik + "/api/v1/db",
    method: 'DELETE'
  };
  request(
    requestParams,
    function(error, res2, data) {
      if(res2.statusCode == 204) {
        console.log("DB was droped");
        res.redirect('/db');
      } else {
        console.log("Error dropping db");
        res.redirect('/db');
      }
    }
  );
};

module.exports.goLive = function(req, res) {
  var reqData, path, reqParams, reqUser;
  path = '/api/v1/playlistsLive/';
  reqUser = req.params.userID;
  //console.log(req.body);
  reqParams = {
    url: apiParametri.streznik + path + reqUser,
    method: 'POST',
    json: {
      stationGenre: req.body.stationGenre,
      stationTag: req.body.stationTag
    }
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 201) {
        //console.log(data);
        res.redirect('/station/'+reqUser);
        return;
      } else if(response.statusCode == 500) {
        // Že obstaja playlist
        res.redirect('/playlist/'+reqUser);
        return;
      } else {
        res.redirect('/playlist/'+reqUser);
        return;
      }   
    }
  );
};

module.exports.goOffline = function(req, res) {
  var reqData, path, reqParams, reqUser;
  path = '/api/v1/playlistsLive/';
  reqUser = req.params.userID;
  //console.log(req.body);
  reqParams = {
    url: apiParametri.streznik + path + reqUser,
    method: 'DELETE',
    json: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      if(response.statusCode == 204) {
        //console.log(data);
        res.redirect('/playlist/'+reqUser);
        return;
      } else {
        res.redirect('/station/'+reqUser);
        return;
      }   
    }
  );
};

var renderStation = function(req, res, data) {
  res.render('station', {
    stationName: data.playlistName,
    videos: data.videos,
    user: req.params.userID
  });
};

module.exports.station = function(req, res) {
  var reqParams, path, userID;
  userID = req.params.userID;
  path = '/api/v1/playlists/';
  reqParams = {
    url: apiParametri.streznik + path + userID,
    method: 'GET',
    json: {},
    qs: {}
  };
  request(
    reqParams,
    function(error, response, data) {
      //console.log(data);
      renderStation(req, res, data);     
    }
  );
};
